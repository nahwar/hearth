class CreatePacks < ActiveRecord::Migration
  def change
    create_table :packs do |t|
      t.string :c1
      t.string :c2
      t.string :c3
      t.string :c4
      t.string :c5
      t.integer :mememaster_id

      t.timestamps null: false
    end
  end
end
