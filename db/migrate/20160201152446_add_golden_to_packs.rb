class AddGoldenToPacks < ActiveRecord::Migration
  def change
    add_column :packs, :c1g, :boolean
    add_column :packs, :c2g, :boolean
    add_column :packs, :c3g, :boolean
    add_column :packs, :c4g, :boolean
    add_column :packs, :c5g, :boolean
  end
end
