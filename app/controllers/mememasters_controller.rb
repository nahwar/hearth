class MememastersController < ApplicationController
	

	def show
  	@user = Mememaster.find(params[:id])
  	@packs = current_mememaster.packs.build
  	getnums

	end

	private

	


	def getnums
		@commons = 0
		@rares = 0
		@mythicals = 0
		@legendaries = 0
		@commonsg = 0
		@raresg = 0
		@mythicalsg = 0
		@legendariesg = 0

		@allpacks = @user.packs.all
		@packscards= (@allpacks.count*5)
		
		for i in @allpacks
			if i.c1 == "common" && i.c1g
				@commonsg += 1
			elsif i.c1 == "common" && !i.c1g
				@commons += 1
			elsif i.c1 == "rare" && i.c1g
				@raresg += 1
			elsif i.c1 == "rare" && !i.c1g
				@rares += 1
			elsif i.c1 == "mythical" && i.c1g
				@mythicalsg += 1
			elsif i.c1 == "mythical" && !i.c1g
				@mythicals += 1
			elsif i.c1 == "legendary" && i.c1g
				@legendariesg += 1
			elsif i.c1 == "legendary" && !i.c1g
				@legendaries += 1
			end
			
			if i.c2 == "common" && i.c2g
				@commonsg += 1
			elsif i.c2 == "common" && !i.c2g
				@commons += 1
			elsif i.c2 == "rare" && i.c2g
				@raresg += 1
			elsif i.c2 == "rare" && !i.c2g
				@rares += 1
			elsif i.c2 == "mythical" && i.c2g
				@mythicalsg += 1
			elsif i.c2 == "mythical" && !i.c2g
				@mythicals += 1
			elsif i.c2 == "legendary" && i.c2g
				@legendariesg += 1
			elsif i.c2 == "legendary" && !i.c2g
				@legendaries += 1
			end
			
			if i.c3 == "common" && i.c3g
				@commonsg += 1
			elsif i.c3 == "common" && !i.c3g
				@commons += 1
			elsif i.c3 == "rare" && i.c3g
				@raresg += 1
			elsif i.c3 == "rare" && !i.c3g
				@rares += 1
			elsif i.c3 == "mythical" && i.c3g
				@mythicalsg += 1
			elsif i.c3 == "mythical" && !i.c3g
				@mythicals += 1
			elsif i.c3 == "legendary" && i.c3g
				@legendariesg += 1
			elsif i.c3 == "legendary" && !i.c3g
				@legendaries += 1
			end
			
			if i.c4 == "common" && i.c4g
				@commonsg += 1
			elsif i.c4 == "common" && !i.c4g
				@commons += 1
			elsif i.c4 == "rare" && i.c4g
				@raresg += 1
			elsif i.c4 == "rare" && !i.c4g
				@rares += 1
			elsif i.c4 == "mythical" && i.c4g
				@mythicalsg += 1
			elsif i.c4 == "mythical" && !i.c4g
				@mythicals += 1
			elsif i.c4 == "legendary" && i.c4g
				@legendariesg += 1
			elsif i.c4 == "legendary" && !i.c4g
				@legendaries += 1
			end
			
			if i.c5 == "common" && i.c5g
				@commonsg += 1
			elsif i.c5 == "common" && !i.c5g
				@commons += 1
			elsif i.c5 == "rare" && i.c5g
				@raresg += 1
			elsif i.c5 == "rare" && !i.c5g
				@rares += 1
			elsif i.c5 == "mythical" && i.c5g
				@mythicalsg += 1
			elsif i.c5 == "mythical" && !i.c5g
				@mythicals += 1
			elsif i.c5 == "legendary" && i.c5g
				@legendariesg += 1
			elsif i.c5 == "legendary" && !i.c5g
				@legendaries += 1
			end
			
		end

		@pcommons = (@commons.to_f/@packscards)*100
		@prares = (@rares.to_f / @packscards) * 100
		@pmythicals = (@mythicals.to_f / @packscards) * 100
		@plegendaries = (@legendaries.to_f / @packscards) * 100

		
	end

end
