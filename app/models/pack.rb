class Pack < ActiveRecord::Base
	belongs_to :mememaster
	validates :c1, presence: true
	validates :c2, presence: true
	validates :c3, presence: true
	validates :c4, presence: true
	validates :c5, presence: true
end
